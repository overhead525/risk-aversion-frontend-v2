export const AUTH_SERVER_URL = "http://localhost:4000/auth";
export const SIM_SERVER_URL = 'http://localhost:3000/graphql';
export const IMAGE_SERVER_URL = 'http://localhost:5050/image';
export const PROFILE_SERVER_URL = 'http://localhost:5050/profile';